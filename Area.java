import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import static java.lang.Thread.sleep;
import java.util.*;
import java.awt.event.*;

public class Area extends JPanel {
    private Color[] colors = {  Color.blue, Color.red, Color.yellow,Color.green, Color.pink,Color.magenta,Color.cyan 
    ,new Color(242,104,119),new Color(191,91,0),new Color(0,170,204),new Color(96,221,73),new Color(255,216,22), new Color(249,58,43),
    new Color(255,0,147),new Color(0,181,155),new Color(209,0,132),new Color(12,28,140)};
    
    public static void main (String [] args) {
        WindowUtilities.openInJFrame(new Area(),600,600,"Funciones Polares");
    }
    
    public void paint (Graphics g) {
        this.setBackground(Color.WHITE);
        Graphics2D g2 = (Graphics2D) g;
        /** Antialiasing */
        RenderingHints rh =
            new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHints(rh);
        // Mover el origen al centro del circulo.
        g2.translate(300.0, 300.0);        
        g2.setColor(Color.BLUE);
        g2.setStroke(new BasicStroke(1f));
        ejes(g2);
        g2.setFont(new Font("Times New Roman",1,25));
        Color c=colors[(int) (Math.random() * 15)+1 ];
        MisTransformaciones tras = new MisTransformaciones();
        g2.setStroke(new BasicStroke(2f));
        dibujar(g2,tras.escalar(calcularPuntos1(),40,40),c);
        g2.drawString("r = 3(1+Senθ)",-280,-250);
        c=colors[(int) (Math.random() * 15)+1 ];
        dibujar(g2,tras.escalar(calcularPuntos2(),40,40),c);
        g2.drawString("r = 3(1-Senθ)",-280,-220);
        
     

        
    }
    
    public void ejes(Graphics2D g2){   
        /** Trazar los ejes de coordenadas */       
        g2.drawLine(-300,0,300,0);
        g2.drawLine(0,-300,0,300);
        for(int i = -300; i <= 300; i+=10){
            g2.drawLine(i,-5,i,5);
        }
        for(int i = -300; i <= 300; i+=10){
            g2.drawLine(-5,i,5,i);
        }
    }
    
    public ArrayList<Point2D> calcularPuntos1(){
        ArrayList<Point2D> puntos= new ArrayList();
         System.out.printf("%10s %30s \n","Coordenadas Rectángulares","Coordenadas Polares");
        for(int i=0;i<=360;i++){
             double limite  = Math.toRadians(i);
             double seno =3*(1+Math.sin(limite));
             //tetha=i
             //r=seno
             double x=seno*Math.cos(limite);
             double y=seno*Math.sin(limite);
             puntos.add(new Point2D(x,y)); 
             System.out.printf("   x: %.3f   y: %.3f %15s r: %.3f  θ: %.3f  \n",x,y,"",seno,limite);
        }
        
        return puntos;
    }
    public ArrayList<Point2D> calcularPuntos2(){
        ArrayList<Point2D> puntos= new ArrayList();
         System.out.printf("%10s %30s \n","Coordenadas Rectángulares","Coordenadas Polares");
        for(int i=0;i<=360;i++){
             double limite  = Math.toRadians(i);
             double seno =3*(1-Math.sin(limite));
             //tetha=i
             //r=seno
             double x=seno*Math.cos(limite);
             double y=seno*Math.sin(limite);
             puntos.add(new Point2D(x,y)); 
            System.out.printf("   x: %.3f   y: %.3f %15s r: %.3f  θ: %.3f  \n",x,y,"",seno,limite);
        }
        
        return puntos;
    }
    public void dibujar(Graphics2D g2,ArrayList puntos,Color c) {
            Point2D p;
            /** Creacion del GeneralPath */
            GeneralPath gp = new GeneralPath();                       
            /** ligar un iterador a la lista de objetos Punto */
            Iterator  it= puntos.iterator();        
            /** colocar el trazador en el primer punto */
            if(it.hasNext()) {
                p = (Point2D) it.next();
                gp.moveTo(p.x(),p.y());
            }
            /** recorrer la lista de objetos Punto y dibujar las lineas */
            while (it.hasNext()) {
                p = (Point2D) it.next();
                gp.lineTo(p.x(),p.y());
            }  
            gp.closePath();
            g2.setColor(c);
            g2.draw(gp);
    }
}
