import javax.swing.*;
import java.awt.*;
import java.awt.geom.*;
import static java.lang.Thread.sleep;
import java.util.*;
import java.awt.event.*;

public class Fig5 extends JPanel{
    private Color[] colors = {  Color.blue, Color.red, Color.yellow,Color.green, Color.pink,Color.magenta,Color.cyan 
    ,new Color(242,104,119),new Color(191,91,0),new Color(0,170,204),new Color(96,221,73),new Color(255,216,22), new Color(249,58,43),
    new Color(255,0,147),new Color(0,181,155),new Color(209,0,132),new Color(12,28,140)};
    
    public static void main (String [] args) {
        WindowUtilities.openInJFrame(new Fig5(),500,500,"Funciones Polares");
    }
    
    public void paint (Graphics g) {
        this.setBackground(Color.WHITE);
        Graphics2D g2 = (Graphics2D) g;
        /** Antialiasing */
        RenderingHints rh =
            new RenderingHints(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            rh.put(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
            g2.setRenderingHints(rh);
        // Mover el origen al centro del circulo.
        g2.translate(250.0, 250.0);        
        /** Establecemos la escala 130:1 (130 px por 1 unidad) */
        g2.setColor(Color.BLUE);
        g2.setStroke(new BasicStroke(1f));
        ejes(g2);
        g2.setColor(Color.BLACK);
        g2.setFont(new Font("Times New Roman",1,40));
        g2.drawString("r = 1+2Cosθ",-230,-190);
        Color c=colors[(int) (Math.random() * 15)+1 ];
         MisTransformaciones tras = new MisTransformaciones();
        g2.setStroke(new BasicStroke(2f));
        dibujar(g2,tras.escalar(calcularPuntos(),70,70),c);
     

        
    }
    
    public void ejes(Graphics2D g2){   
        /** Trazar los ejes de coordenadas */       
        g2.drawLine(-250,0,250,0);
        g2.drawLine(0,-250,0,250);
        for(int i = -250; i <= 250; i+=10){
            g2.drawLine(i,-5,i,5);
        }
        for(int i = -250; i <= 250; i+=10){
            g2.drawLine(-5,i,5,i);
        }
    }
    
    public ArrayList<Point2D> calcularPuntos(){
        ArrayList<Point2D> puntos= new ArrayList();
         System.out.printf("%10s %30s \n","Coordenadas Rectángulares","Coordenadas Polares");
        for(float i=0.5f;i<=360;i=i+0.5f){
             double limite  = Math.toRadians(i);
             double coseno =1+2*(Math.cos(limite));
             //tetha=i
             //r=coseno
             double x=coseno*Math.cos(limite);
             double y=coseno*Math.sin(limite);
             puntos.add(new Point2D(x,y)); 
            System.out.printf("   x: %.3f   y: %.3f %15s r: %.3f  θ: %.3f  \n",x,y,"",coseno,limite);
        }
        
        return puntos;
    }
    public void dibujar(Graphics2D g2,ArrayList puntos,Color c) {
            Point2D p;
            /** Creacion del GeneralPath */
            GeneralPath gp = new GeneralPath();                       
            /** ligar un iterador a la lista de objetos Punto */
            Iterator  it= puntos.iterator();        
            /** colocar el trazador en el primer punto */
            if(it.hasNext()) {
                p = (Point2D) it.next();
                gp.moveTo(p.x(),p.y());
            }
            /** recorrer la lista de objetos Punto y dibujar las lineas */
            while (it.hasNext()) {
                p = (Point2D) it.next();
                gp.lineTo(p.x(),p.y());
            }  
            gp.closePath();
            g2.setColor(c);
            g2.draw(gp);
    }
}