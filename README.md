# Gráficas polares

## Indicaciones

El proyecto de las gráficas polares, contiene 12 clases para ejecutar (Rosa, Rosa2, Rosa4, Rosa3, Fig5, Fig4,Fig3, Fig2, Fig1, Fig, Area2, Area). Al ejecutar cualquiera de estas clases aparecerá la figura trazada y una ventana en consola con la información de los puntos que componen la figura, se muestran las coordenadas polares y su respectiva equivalencia en coordenadas rectangulares.

## Imágenes del sistema

Las imágenes se encuentran en la carpeta Capturas del Programa.
