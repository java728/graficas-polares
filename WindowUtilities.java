import java.awt.Container;
import javax.swing.JFrame;
import java.awt.Color;

public class WindowUtilities {
    public static JFrame openInJFrame(Container content, int width, int height,
                String title) {
       JFrame frame = new JFrame(title);
       frame.add(content);
       frame.setSize(width, height);
       frame.setBackground(Color.WHITE);
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setResizable(false);
       frame.setLocationRelativeTo(null);
       frame.setVisible(true);
       return (frame);
    }
}
