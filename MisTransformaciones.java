import java.util.*;

public class MisTransformaciones {
    
    public ArrayList<Point2D> trazarlPoligono(double x,double y, int nAristas) {
        /** copiamos las coordenadas iniciales */
        double x1 = x, y1 = y;
        
        /** creamos la lista de puntos */
        ArrayList<Point2D> puntosO = new ArrayList();
        
        /** calculamos el angulo de rotacion en radianes */
        double ang = Math.toRadians(360.0 / nAristas);
        
        /** Agregamos a la lista las coordenadas iniciales */
        puntosO.add(new Point2D(x1, y1));
        int nA = 1;
        
        /** calculamos las coordenadas siguientes */
        while (nA < nAristas) {
            x1 = x * Math.cos(ang) - y * Math.sin(ang);
            y1 = x * Math.sin(ang) + y * Math.cos(ang);
            puntosO.add(new Point2D(x1, y1));
            
            x  = x1;
            y  = y1;
            nA++;
        }        
        return puntosO;
    }
    
    public  ArrayList rotar(ArrayList<Point2D> puntos, double ang) {
        ListIterator lit = puntos.listIterator();
        ArrayList<Point2D> puntosR= new ArrayList();
        double x1, y1, x, y;
        Point2D p;
        /** calculamos el angulo de rotacion en radianes */
        double ang1 = Math.toRadians(ang);
      
        /** generamos los nuevos puntos */
        while(lit.hasNext())
        { 
            p  = (Point2D) lit.next();
            x  = p.x();
            y  = p.y();
            x1 = x * Math.cos(ang1) - y * Math.sin(ang1);
            y1 = x * Math.sin(ang1) + y * Math.cos(ang1);
           puntosR.add(new Point2D(x1,y1));              
        }
        
        return puntosR;
    }
    
    /**
     * Rotación a través de un punto pivote
     */public  ArrayList rotar(ArrayList<Point2D> puntos, double ang,Point2D punto) {
        ListIterator lit = puntos.listIterator();
        ArrayList<Point2D> puntosR= new ArrayList();
        double x1, y1, x, y,xr,yr;
        xr=punto.x();
        yr=punto.y();
        Point2D p;
        /** calculamos el angulo de rotacion en radianes */
        double ang1 = Math.toRadians(ang);
      
        /** generamos los nuevos puntos */
        while(lit.hasNext())
        { 
            p  = (Point2D) lit.next();
            x  = p.x();
            y  = p.y();
            x1 = xr+(x-xr) * Math.cos(ang1) - (y-yr) * Math.sin(ang1);
            y1 = (x-xr) * Math.sin(ang1) + (y-yr) * Math.cos(ang1)+yr;
           puntosR.add(new Point2D(x1,y1));              
        }
        
        return puntosR;
    }
    
    public  ArrayList trasladar(ArrayList<Point2D> puntos, double tx, double ty) {
        /** copiamos las coordenadas iniciales */        
        ListIterator lit = puntos.listIterator();
        ArrayList<Point2D> puntosT= new ArrayList();
        double x1, x, y1, y;
        Point2D p;
        
        /** generamos los nuevos puntos */
        while(lit.hasNext())
        { 
            p  = (Point2D) lit.next();
            x  = p.x();
            y  = p.y();
            x1 = x + tx;
            y1 = y + ty;
            puntosT.add(new Point2D(x1, y1));
        }
        
        return puntosT;
    }
     
    public  ArrayList escalar(ArrayList puntos, double ex, double ey) {
        /** copiamos las coordenadas iniciales */
        double x1, x, y1, y;
        ArrayList<Point2D> puntosE= new ArrayList();
        Point2D p;
        /** generamos los nuevos puntos */
        ListIterator lit = puntos.listIterator();
        while(lit.hasNext())
        {   
            p  = (Point2D) lit.next();
            x  = p.x();
            y  = p.y();
            x1 = x * ex;
            y1 = y * ey;
            puntosE.add(new Point2D(x1, y1));
        }
        
        return puntosE;
    }
        
    public  ArrayList sesgar(ArrayList<Point2D> puntos, double a) {
        /** copiamos las coordenadas iniciales */
        double x1, x, y1, y;
        Point2D p;
        ArrayList<Point2D> puntosS= new ArrayList();
        /** generamos los nuevos puntos */
        ListIterator lit = puntos.listIterator();
        while(lit.hasNext())
        {   
            p  = (Point2D)lit.next();
            x  = p.x();
            y  = p.y();
            x1 = x+(a*y);
            y1 = y;
            puntosS.add(new Point2D(x1, y1));
        }
        
        return puntosS;
    }
}
